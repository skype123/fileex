package com.netty.mvc;


import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER ,ElementType.FIELD})
public @interface MyAutowired {
}

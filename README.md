# fileex

#### 介绍
针对大文件的分块传输和断点续传设计实现的处理器（支持上传和下载），同时具备上传/下载进度和上传/下载速度的推送功能。
本项目的前端项目和客户端项目并不是单独部署到一台服务器的，是在本机启动，因为使用前端页面与服务端实现大文件传输协议对于我来说有点困难...
而且对编写java桌面客户端没有实战过。所以前端项目和客户端项目在本机启动，读取本机系统的文件，并提供页面操作

建议和交流QQ：807758751

#### 软件架构
1. frontend-service 简单实现的前端页面（依赖netty-client）
2. netty-client netty客户端 (依赖netty-common)
3. netty-common netty客户端服务端公共项目
4. netty-server netty服务端 (依赖netty-common)
5. mongodb 提供数据服务（上传任务、下载任务、已上传完的文件数据）

#### 技术实现
1. 分块传输使用HTTP1.1协议的transfer-encoding:chunked，netty客户端和服务端上添加

```
pipeline.addLast("chunkedWriter", new ChunkedWriteHandler());
```

2. 断点续传主要使用了RandomAccessFile类来实现，不过该类是大量磁盘IO操作，所以效率不太高。
   客户端首次上传文件时，不会使用RandomAccessFile进行分割，若此时停止上传再继续上传，客户端会首先请求服务端获取当前文件上传多少长度，客    
   户端得到该长度会使用RandomAccessFile进行分割文件得到新的文件再次调用上传接口进行上传。
   下载同样道理，只是是由客户端得到当前文件已下载的长度推给服务端，服务端拿到长度进行RandomAccessFile分割文件在推给客户端.

3. netty-server 由于没有springboot的mvc功能，在编写接口上有一定的麻烦程度，所以这里简单手写实现了mvc的功能
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/113247_c4860ecf_621372.png "屏幕截图.png")

4. 由于直接默认使用netty的文件上传，是可以支持分块传输，但是不支持断点续传。所以这里改写了一下netty在接收文件写入文件时的部分代码
   详细见netty-server/src/main/java/com/netty/my/MyAbstractDiskHttpData.java,netty-server/src/main/java/com/netty/my/MyDefaultHttpDataFactory.java,netty-server/src/main/java/com/netty/my/MyDiskFileUpload.java

#### 安装教程

1.  前端客户端连接服务端的配置类在netty-client/src/main/java/com/netty/Constants.java中
![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/105054_2b6b8181_621372.png "屏幕截图.png")
SERVER_IP 为netty-server所在服务器ip，WEBSOCKET_PORT为netty-server的websocket端口
2.  本机启动前端客户端frontend-service，一个springboot服务
3.  服务器安装mongodb
4.  netty-server的配置类在netty-client/src/main/java/com/netty/Constants.java
![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/105713_4058072a_621372.png "屏幕截图.png")
注意WEBSOCKET_IP只能填写0.0.0.0
5.  服务器部署netty-server服务  启动命令java -jar xxxx.jar即可

#### 使用说明

1.  浏览器进入前端页面http://localhost:8789/frontend/file
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/105947_7794eeba_621372.png "屏幕截图.png")
2.  左上角的圆角加号可以创建根目录
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/110015_0f3c3c0a_621372.png "屏幕截图.png")
3.  每个目录右边的加号可以创建该目录的子目录
4.  点击目录，可以显示该目录下已经上传成功的文件
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/110105_bacc38fc_621372.png "屏幕截图.png")
5.  点击文件上传按钮，弹窗显示本机系统的目录和文件架构
![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/134153_ef58a69d_621372.png "屏幕截图.png")
6.  选中某个文件，点击下方确定按钮即可对该文件进行分块传输
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/110237_921866dc_621372.png "屏幕截图.png")
   同时支持多文件上传（当然了这是要看服务器和本机性能配置是否给力...）
7.  上传中心支持断点续传,即点击暂停，暂停后可点击继续或取消上传
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/110351_ad333d2c_621372.png "屏幕截图.png")

8. 文件上传成功后会刷新文件列表

9. 文件列表的每个文件的下载按钮会弹出选择要下载到本机指定目录
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/110502_83b4cb82_621372.png "屏幕截图.png")
  选中目录后 点击下方确定按钮，即可进入下载中心进行文件的下载

10. 下载中心跟上传中心一样 支持断点续传
![输入图片说明](https://images.gitee.com/uploads/images/2020/0911/130502_2e560472_621372.png "屏幕截图.png")
  
